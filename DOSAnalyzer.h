//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_DOSANALYZER_H
#define ITAK_DOSANALYZER_H


#include "Analyzer.h"

class DOSAnalyzer : public Analyzer
{
private:
    std::vector<std::pair<std::string,std::pair<std::vector<unsigned int>,int>>> m_addressToSummary;
    void createPair(int,std::vector<inputStream>&) override;
public:
    DOSAnalyzer(){};
    ResultSet run(std::vector<inputStream>&) override;
};


#endif //ITAK_DOSANALYZER_H
