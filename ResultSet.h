//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_RESULTSET_H
#define ITAK_RESULTSET_H

#include <vector>
#include <map>

class ResultSet
{
private:
    std::vector<std::pair<std::string,std::vector<std::string>>> m_results;
public:
    void print(std::ostream&) const;
    void setKey(std::string);
    void addValue(std::string,std::string);
    void addValue(std::string, unsigned int);
};


#endif //ITAK_RESULTSET_H
