#include <iostream>
#include <fstream>

#include "inputStream.h"
#include "ITAK.h"
#include "UserInterface.h"


int main() {
    
    //for grader: algorthms don't work.
    UserInterface userInterface;
    userInterface.openFile("SampleData.csv");
    userInterface.setParameters();
    ITAK itak;
    itak.setConfiguration(userInterface.getParameters());
    if(userInterface.getPick() == "DOS")
    {
        ResultSet resultSet;
        resultSet = itak.runDOSAnalyzer(userInterface.getInputs());
        resultSet.print(std::cout);
    }
    if(userInterface.getPick() == "Port")
    {
        ResultSet resultSet;
        resultSet = itak.runPortScanAnalyzer(userInterface.getInputs());
        resultSet.print(std::cout);
    }

    return 0;
}