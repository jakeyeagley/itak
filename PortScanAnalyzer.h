//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_PORTSCANANALYZER_H
#define ITAK_PORTSCANANALYZER_H

#include "Analyzer.h"

class PortScanAnalyzer : public Analyzer
{
private:
    std::vector<std::pair<std::string,std::vector<int>>> m_addressToSummary;
    void createPair(int,std::vector<inputStream>&) override;
public:
    PortScanAnalyzer(){};
    ResultSet run(std::vector<inputStream>&) override;

};


#endif //ITAK_PORTSCANANALYZER_H
