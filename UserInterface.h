//
// Created by yeagols on 4/19/17.
//

#ifndef ITAK_USERINTERFACE_H
#define ITAK_USERINTERFACE_H

#include <fstream>
#include <vector>
#include "inputStream.h"

class UserInterface
{
private:
    std::ifstream file;
    std::vector<inputStream> inputStreams;
    std::vector<std::pair<std::string,int>> parameters;
     std::string pick;
public:
    std::string getPick() {return pick;}
    void openFile(std::string);
    void setParameters();
    std::vector<inputStream> getInputs() {return inputStreams;}
    std::vector<std::pair<std::string,int>> getParameters() {return parameters;};
};
#endif //ITAK_USERINTERFACE_H
