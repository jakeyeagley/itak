//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_ITAK_H
#define ITAK_ITAK_H


#include "inputStream.h"
#include "Configuration.h"
#include "DOSAnalyzer.h"
#include "PortScanAnalyzer.h"
#include "iostream"

class ITAK {
private:
    Configuration m_configuration;
    DOSAnalyzer m_DOSAnalyer;
    PortScanAnalyzer m_PortScanAnalyzer;
public:
    ITAK(){};
    const Configuration getConfiguration(){return m_configuration;}
    void setConfiguration(std::vector<std::pair<std::string,int>>);
    ResultSet runDOSAnalyzer(std::vector<inputStream>);
    ResultSet runPortScanAnalyzer(std::vector<inputStream>);
};


#endif //ITAK_ITAK_H
