//
// Created by yeagols on 4/11/17.
//

#include "ITAK.h"

void ITAK::setConfiguration(std::vector<std::pair<std::string,int>> configs)
{
    m_configuration.setConfiguration(configs);
}

ResultSet ITAK::runDOSAnalyzer(std::vector<inputStream> inputs)
{
    ResultSet resultSet;
    m_DOSAnalyer.setConfiguration(m_configuration);
    resultSet = m_DOSAnalyer.run(inputs);

    return resultSet;
}

ResultSet ITAK::runPortScanAnalyzer(std::vector<inputStream> inputs)
{
    ResultSet resultSet;
    m_PortScanAnalyzer.setConfiguration(m_configuration);
    resultSet = m_PortScanAnalyzer.run(inputs);

    return resultSet;
}