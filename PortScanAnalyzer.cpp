//
// Created by yeagols on 4/11/17.
//

#include "PortScanAnalyzer.h"

ResultSet PortScanAnalyzer::run(std::vector<inputStream>& inputStream1)
{
    ResultSet resultSet;
    resultSet.setKey("likely attackers");
    resultSet.setKey("possible attackers");
    resultSet.setKey("port count");

    for(auto i = 0; i < inputStream1.size(); i++)
    {
        for(auto j = 0; j < m_addressToSummary.size(); j++)
        {
            if (inputStream1[i].getSrcAddress() == m_addressToSummary[j].first)
            {
                m_addressToSummary[j].second.push_back(convertStringToUnsignedInt(inputStream1[i].getDesPort()));
            }
            else
            {
                createPair(i,inputStream1);
            }
        }
        if(m_addressToSummary.size() == 0)
        {
            createPair(i,inputStream1);
        }
    }
    for(auto i = 0; i < m_addressToSummary.size(); i++)
    {
        if(m_addressToSummary[i].second.size() >= m_configuration.getParameter("likelyAttackPortCount"))
        {
            resultSet.addValue("likely attackers", inputStream1[i].getSrcAddress());
        }
        else if(m_addressToSummary[i].second.size() >= m_configuration.getParameter("possibleAttackPortCount"))
        {
            resultSet.addValue("possible attackers", inputStream1[i].getSrcAddress());
        }
    }
    
    return resultSet;
}

void PortScanAnalyzer::createPair(int i,std::vector<inputStream>& inputStream1)
{
    //create container for destination ports
    std::vector<int> container;
    container.push_back(convertStringToUnsignedInt(inputStream1[i].getDesPort()));
    //create pair with IP and des port
    std::pair<std::string,std::vector<int>> pair;
    pair.first = inputStream1[i].getSrcAddress();
    pair.second = container;

    m_addressToSummary.push_back(pair);

};
