//
// Created by yeagols on 4/11/17.
//

#include <iostream>
#include "Configuration.h"

void Configuration::setConfiguration(std::vector<std::pair<std::string,int>> configs)
{
    m_configs = configs;
}

int Configuration::getParameter(std::string key) const
{
    for(auto i = 0; i < m_configs.size(); i++)
    {
        if(m_configs[i].first == key)
            return m_configs[i].second;
    }
    throw std::string("error");
}