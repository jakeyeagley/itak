//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_ANALYZER_H
#define ITAK_ANALYZER_H


#include "ResultSet.h"
#include "inputStream.h"
#include "iostream"
#include "Configuration.h"

class Analyzer {
private:
public:
    void setConfiguration(Configuration);
    Configuration getConfiguration() {return m_configuration;};
protected:
    Configuration m_configuration;
    virtual ResultSet run(std::vector<inputStream>&) = 0;
    virtual void createPair(int, std::vector<inputStream>&) = 0;
};


#endif //ITAK_ANALYZER_H
