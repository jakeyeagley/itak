//
// Created by yeagols on 4/11/17.
//

#include "DOSAnalyzer.h"

ResultSet DOSAnalyzer::run(std::vector<inputStream>& inputStream1)
{
    ResultSet resultSet;
    resultSet.setKey("likely attackers");
    resultSet.setKey("possible attackers");
    resultSet.setKey("attack period");
    resultSet.setKey("Timeframe");

    for(auto i = 0; i < inputStream1.size() - 1; i++)
    {
        for(auto j = 0; j < m_addressToSummary.size(); j++)
        {
            if (inputStream1[i].getSrcAddress() == m_addressToSummary[j].first)
            {
                m_addressToSummary[j].second.first.push_back(inputStream1[i].getTimeStamp());
                m_addressToSummary[j].second.second++;
            }
            else
            {
                createPair(i,inputStream1);
                break;
            }
        }
        if(m_addressToSummary.size() == 0)
        {
            createPair(i,inputStream1);
        }
    }

    for(auto i = 0; i < m_addressToSummary.size() - 1; i++)
    {
        for(auto j = 0; j < m_addressToSummary[i].second.first.size() - 1; j++)
        {
            if (m_addressToSummary[i].second.first[j] >= m_configuration.getParameter("likelyThreshold"))
            {
                resultSet.addValue("likely attackers", inputStream1[i].getSrcAddress());
                resultSet.addValue("attack period", inputStream1[i].getTimeStamp());
            }
            else if(m_addressToSummary[i].second.first[j] >= m_configuration.getParameter("possibleThreshold"))
            {
                resultSet.addValue("possibleattackers", inputStream1[i].getSrcAddress());
                resultSet.addValue("attack period", inputStream1[i].getTimeStamp());
            }
        }
    }

    return resultSet;
}

void DOSAnalyzer::createPair(int i,std::vector<inputStream>& inputStream1)
{
    std::vector<unsigned int> vect;
    vect.push_back(inputStream1[i].getTimeStamp());

    std::pair<std::vector<unsigned int>,int> timeStampToDictionary;
    timeStampToDictionary = std::make_pair(vect,1);

    std::pair<std::string, std::pair<std::vector<unsigned int>, int>> addressToSummary;
    addressToSummary = std::make_pair(inputStream1[i].getSrcAddress(),timeStampToDictionary);

    m_addressToSummary.push_back(addressToSummary);
}