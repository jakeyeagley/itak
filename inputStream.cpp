//
// Created by yeagols on 4/11/17.
//

#include "inputStream.h"

void inputStream::setInputStream(std::string input)
{
    //TODO need to test validation
    std::string elements[4];
    split(input,',',elements,4);
    m_timeStamp = convertStringToUnsignedInt(elements[0]);
    m_srcAddress = elements[1];
    m_srcPort = elements[2];
    m_desPort = elements[3];
}