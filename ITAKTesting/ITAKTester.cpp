//
// Created by yeagols on 4/11/17.
//

#include "ITAKTester.h"
#include "../ITAK.h"

void ITAKTester::testSetConfiguration()
{
    ITAK itak;
    Configuration configuration;
    std::vector<std::pair<std::string,int>> testVector;
    std::pair<std::string,int> testPair1;

    testPair1.first = "TimeFrame";
    testPair1.second = 100;
    testVector.push_back(testPair1);

    std::pair<std::string,int> testPair2;
    testPair2.first = "LikelyAttackMessageCount";
    testPair2.second = 4;
    testVector.push_back(testPair2);

    std::pair<std::string,int> testPair3;
    testPair3.first = "PossibleAttackMessageCount";
    testPair3.second = 7;
    testVector.push_back(testPair3);

    itak.setConfiguration(testVector);

    if(itak.getConfiguration().getParameter("TimeFrame") != 100)
    {
        std::cout << "error in ITAK.setConfiguration unexpected value of: "
                  << itak.getConfiguration().getParameter("TimeFrame")
                  << std::endl;
    }

    if(itak.getConfiguration().getParameter("LikelyAttackMessageCount") != 4)
    {
        std::cout << "error in ITAK.setConfiguration unexpected value of: "
                  << itak.getConfiguration().getParameter("LikelyAttackMessageCount")
                  << std::endl;
    }

    if(itak.getConfiguration().getParameter("PossibleAttackMessageCount") != 7)
    {
        std::cout << "error in ITAK.setConfiguration unexpected value of: "
                  << itak.getConfiguration().getParameter("PossibleAttackMessageCount")
                  << std::endl;
    }
}