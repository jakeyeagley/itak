//
// Created by yeagols on 4/11/17.
//

#include <iostream>
#include "../inputStream.h"
#include "../Configuration.h"
#include "ConfigurationTester.h"
#include "DOSAnalyzerTester.h"
#include "inputStreamTester.h"
#include "ITAKTester.h"
#include "PortScanAnalyzerTester.h"

int main()
{
    std::cout <<"testing" << std::endl;
    inputStreamTester inputStreamTester1;
    inputStreamTester1.testSetInputStream();

    ConfigurationTester configurationTester;
    configurationTester.testGetParameter();

    ITAKTester itakTester;
    itakTester.testSetConfiguration();

    DOSAnalyzerTester dosAnalyzerTester;
    dosAnalyzerTester.testSetConfiguration();
    dosAnalyzerTester.testRun();

    PortScanAnalyzerTester portScanAnalyzerTester;
    portScanAnalyzerTester.testSetConfiguration();
    portScanAnalyzerTester.testRun();

    return 0;
}