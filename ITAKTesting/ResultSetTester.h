//
// Created by yeagols on 4/19/17.
//

#ifndef ITAK_RESULTSETTESTER_H
#define ITAK_RESULTSETTESTER_H


class ResultSetTester {
    void testPrint();
    void testSetKey();
    void testAddValue();
};


#endif //ITAK_RESULTSETTESTER_H
