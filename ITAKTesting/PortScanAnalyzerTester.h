//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_PORTSCANANALYZERTESTER_H
#define ITAK_PORTSCANANALYZERTESTER_H


class PortScanAnalyzerTester {
public:
    void testSetConfiguration();
    void testRun();
};


#endif //ITAK_PORTSCANANALYZERTESTER_H
