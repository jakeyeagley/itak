//
// Created by yeagols on 4/11/17.
//

#ifndef ITAK_CONFIGURATION_H
#define ITAK_CONFIGURATION_H

#include <map>
#include <vector>
#include "Utils.h"
#include "inputStream.h"

class Configuration
{
private:
    std::vector<std::pair<std::string,int>> m_configs;
public:
    void setConfiguration(std::vector<std::pair<std::string,int>>);
    int getParameter(std::string) const;
};


#endif //ITAK_CONFIGURATION_H
